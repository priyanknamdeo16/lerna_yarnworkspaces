import * as React from "react";


export interface HeaderProps {
  children?: any;
}

export class Header extends React.Component<HeaderProps> {
  render() {
    const { children } = this.props;
    const styles = {
      header: {
        height: 54,
        display: "flex",
        flexDirection: "row" as "row",
        alignItems: "center",
        padding: 8,
        backgroundColor: "#ccc",
        color: "red"
      },
      heading: {
        fontSize: 20,
        fontWeight: "normal" as "normal"
      }
    };

    return (
      <div style={styles.header}>
        <img src={require('./photo.jpg')} />
        <h1 style={styles.heading}>{children}</h1>
      </div>
    );
  }
}
