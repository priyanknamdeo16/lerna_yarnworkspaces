var path = require("path");
const webpack = require("webpack");
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

var config = {
  entry: {
    "index": ["./src/index.ts"],
    "components/header" : ["./src/components/header.tsx"]
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/
      },
      {
        test: /\.(jpe?g|png|gif)$/,
        loader: 'url-loader',
        options: {
          // Images larger than 10 KB won’t be inlined
          limit: 10 * 1024
        }
      }
    ]
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js", ".png", ".jpg", ".json"]
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, "dist"),
    library: 'Shared',
    libraryTarget: 'umd'
  },
  plugins: [
    new TsconfigPathsPlugin({ configFile: "./tsconfig.json"}),
  ]
};

module.exports = config;