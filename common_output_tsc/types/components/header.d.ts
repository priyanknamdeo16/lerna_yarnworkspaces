import * as React from "react";
export interface HeaderProps {
    children?: any;
}
export declare class Header extends React.Component<HeaderProps> {
    render(): JSX.Element;
}
